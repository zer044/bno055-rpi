/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

/* 
 * File:   BNO055.cpp
 * Author: narshil.vaghjiani
 * 
 * Created on 02 May 2018, 11:13
 */

#include <stdint.h>
#include <wiringPi.h>
#include <iostream>

#include "BNO055.h"
#include "I2CInterface.h"
#include "utility/imumaths.h"

using namespace bno;

BNO055::BNO055()
{
}

BNO055::~BNO055()
{
}

bool BNO055::begin(adafruit_bno055_opmode_t mode)
{

    // Make sure we have the right device 
    uint8_t id = read8(BNO055_CHIP_ID_ADDR);
    if (id != BNO055_ID)
    {
        delay(100); // hold on for boot
        id = read8(BNO055_CHIP_ID_ADDR);
        if (id != BNO055_ID)
        {
            std::cout << "Expected " << BNO055_ID << " got: " << (int) id << std::endl;
            return false; // still not? ok bail
        }
    }

    // Switch to config mode (just in case since this is the default) 
    setMode(OPERATION_MODE_CONFIG);

    /* Reset */
    write8(BNO055_SYS_TRIGGER_ADDR, 0x20);
    while (read8(BNO055_CHIP_ID_ADDR) != BNO055_ID)
    {
        delay(10);
    }
    delay(50);

    /* Set to normal power mode */
    write8(BNO055_PWR_MODE_ADDR, POWER_MODE_NORMAL);
    delay(10);

    write8(BNO055_PAGE_ID_ADDR, 0);

    write8(BNO055_SYS_TRIGGER_ADDR, 0x0);
    delay(10);
    /* Set the requested operating mode (see section 3.3) */
    setMode(mode);
    delay(20);

    return true;

}

/**************************************************************************/
/*!
    @brief  Puts the chip in the specified operating mode
 */

/**************************************************************************/
void BNO055::setMode(adafruit_bno055_opmode_t mode)
{
    mode_ = mode;
    write8(BNO055_OPR_MODE_ADDR, mode_);
    delay(30);
}


/**************************************************************************/
/*!
    @brief  Changes the chip's axis remap
 */

/**************************************************************************/
void BNO055::setAxisRemap(adafruit_bno055_axis_remap_config_t remapcode)
{
    adafruit_bno055_opmode_t modeback = mode_;

    setMode(OPERATION_MODE_CONFIG);
    delay(25);
    write8(BNO055_AXIS_MAP_CONFIG_ADDR, remapcode);
    delay(10);
    /* Set the requested operating mode (see section 3.3) */
    setMode(modeback);
    delay(20);
}


/**************************************************************************/
/*!
    @brief  Changes the chip's axis signs
 */

/**************************************************************************/
void BNO055::setAxisSign(adafruit_bno055_axis_remap_sign_t remapsign)
{
    adafruit_bno055_opmode_t modeback = mode_;

    setMode(OPERATION_MODE_CONFIG);
    delay(25);
    write8(BNO055_AXIS_MAP_SIGN_ADDR, remapsign);
    delay(10);
    /* Set the requested operating mode (see section 3.3) */
    setMode(modeback);
    delay(20);
}


/**************************************************************************/
/*!
    @brief  Use the external 32.768KHz crystal
 */

/**************************************************************************/
void BNO055::setExtCrystalUse(bool usextal)
{
    adafruit_bno055_opmode_t modeback = mode_;

    /* Switch to config mode (just in case since this is the default) */
    setMode(OPERATION_MODE_CONFIG);
    delay(25);
    write8(BNO055_PAGE_ID_ADDR, 0);
    if (usextal)
    {
        write8(BNO055_SYS_TRIGGER_ADDR, 0x80);
    }
    else
    {
        write8(BNO055_SYS_TRIGGER_ADDR, 0x00);
    }
    delay(10);
    /* Set the requested operating mode (see section 3.3) */
    setMode(modeback);
    delay(20);
}

/**************************************************************************/
/*!
    @brief  Gets current calibration state.  Each value should be a uint8_t
            pointer and it will be set to 0 if not calibrated and 3 if
            fully calibrated.
 */

/**************************************************************************/
void BNO055::getCalibration(uint8_t* sys, uint8_t* gyro, uint8_t* accel, uint8_t* mag)
{
    uint8_t calData = read8(BNO055_CALIB_STAT_ADDR);
    if (sys != NULL)
    {
        *sys = (calData >> 6) & 0x03;
    }
    if (gyro != NULL)
    {
        *gyro = (calData >> 4) & 0x03;
    }
    if (accel != NULL)
    {
        *accel = (calData >> 2) & 0x03;
    }
    if (mag != NULL)
    {
        *mag = calData & 0x03;
    }

    return;
}

bool BNO055::getEvent(sensors_event_t *event)
{
    /* Clear the event */
    memset(event, 0, sizeof (sensors_event_t));

    event->version = sizeof (sensors_event_t);
    event->sensor_id = BNO055_ADDRESS_B;
    event->type = -1;
    event->timestamp = millis();

    /* Get a Euler angle sample for orientation */

    imu::Vector<3> euler = getVector(VECTOR_EULER);
    event->orientation.x = euler.x();
    event->orientation.y = euler.y();
    event->orientation.z = euler.z();


    //  /*Get Raw Compass Data*/
    if (mode_ > OPERATION_MODE_IMUPLUS)
    {
        imu::Vector<3> magRaw = getVector(VECTOR_MAGNETOMETER);
        event->magnetic.x = magRaw.x();
        event->magnetic.y = magRaw.y();
        event->magnetic.z = magRaw.z();
    }

    return true;
}

//**************IMU****************************//
/**************************************************************************/
/*!
    @brief  Gets a vector reading from the specified source
 */

/**************************************************************************/
imu::Vector<3> BNO055::getVector(adafruit_vector_type_t vector_type)
{
    imu::Vector<3> xyz;
    uint8_t buffer[6];
    memset(buffer, 0, 6);

    int16_t x, y, z;
    x = y = z = 0;

    /* Read vector data (6 bytes) */
    readLen((adafruit_bno055_reg_t) vector_type, buffer, 6);

    x = ((int16_t) buffer[0]) | (((int16_t) buffer[1]) << 8);
    y = ((int16_t) buffer[2]) | (((int16_t) buffer[3]) << 8);
    z = ((int16_t) buffer[4]) | (((int16_t) buffer[5]) << 8);

    /* Convert the value to an appropriate range (section 3.6.4) */
    /* and assign the value to the Vector type */
    switch (vector_type)
    {
    case VECTOR_MAGNETOMETER:
        /* 1uT = 16 LSB */
        xyz[0] = ((double) x) / 16.0;
        xyz[1] = ((double) y) / 16.0;
        xyz[2] = ((double) z) / 16.0;
        break;
    case VECTOR_GYROSCOPE:
        /* 1dps = 16 LSB */
        xyz[0] = ((double) x) / 16.0;
        xyz[1] = ((double) y) / 16.0;
        xyz[2] = ((double) z) / 16.0;
        break;
    case VECTOR_EULER:
        /* 1 degree = 16 LSB */
        xyz[0] = ((double) x) / 16.0;
        xyz[1] = ((double) y) / 16.0;
        xyz[2] = ((double) z) / 16.0;
        break;
    case VECTOR_ACCELEROMETER:
    case VECTOR_LINEARACCEL:
    case VECTOR_GRAVITY:
        /* 1m/s^2 = 100 LSB */
        xyz[0] = ((double) x) / 100.0;
        xyz[1] = ((double) y) / 100.0;
        xyz[2] = ((double) z) / 100.0;
        break;
    }

    return xyz;
}

//*********************************************PRIVATE**************//

uint8_t BNO055::read8(adafruit_bno055_reg_t reg)
{
    return I2CreadByte_(bno::BNO055_ADDRESS_B, reg);
}

bool BNO055::write8(adafruit_bno055_reg_t reg, uint8_t value)
{
    return I2CwriteByte_(bno::BNO055_ADDRESS_B, reg, value);
}

bool BNO055::readLen(adafruit_bno055_reg_t reg, uint8_t* buffer, uint8_t len)
{
    I2CreadBytes_(BNO055_ADDRESS_B, reg, buffer, len);
}

uint8_t BNO055::I2CreadByte_(uint8_t address, uint8_t subAddress)
{
    uint8_t data;
    uint8_t addr = address << 1;
    I2CInterface::theI2CInterface().receive(reinterpret_cast<unsigned char> (addr), reinterpret_cast<unsigned char> (subAddress),
                                            reinterpret_cast<unsigned char*> (&data), sizeof (data));


    return data; // Return data read from slave register
}

uint8_t BNO055::I2CreadBytes_(uint8_t address, uint8_t subAddress, uint8_t * dest, uint8_t count)
{
    uint8_t addr = address << 1;

    uint8_t temp_dest[count];
    I2CInterface::theI2CInterface().receive(addr, subAddress, reinterpret_cast<unsigned char*> (&temp_dest), sizeof (temp_dest));

    for (int i = 0; i < count; i++)
    {
        dest[i] = temp_dest[i];
    }

    return count;
}

bool BNO055::I2CwriteByte_(uint8_t address, uint8_t subAddress, uint8_t data)
{
    uint8_t addr = address << 1;

    // build buffer
    unsigned char i2cBuf[2];
    i2cBuf[0] = static_cast<unsigned char> (subAddress);
    i2cBuf[1] = static_cast<unsigned char> (data);

    return I2CInterface::theI2CInterface().send(addr, i2cBuf, sizeof (i2cBuf));
}