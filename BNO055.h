/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

/* 
 * File:   BNO055.h
 * Author: narshil.vaghjiani
 *
 * Created on 02 May 2018, 11:13
 */

#ifndef BNO055_H
#define BNO055_H

#include <cstdint>
#include <stdint.h>
#include "BNO55Data.h"
#include "utility/vector.h"


using namespace bno;

class BNO055 {
public:
    BNO055();
    virtual ~BNO055();

    bool begin(adafruit_bno055_opmode_t mode = OPERATION_MODE_NDOF);
    void setMode(adafruit_bno055_opmode_t mode);
    void setAxisRemap(adafruit_bno055_axis_remap_config_t remapcode);
    void setAxisSign(adafruit_bno055_axis_remap_sign_t remapsign);
    //   void getRevInfo(adafruit_bno055_rev_info_t*);
    //   void displayRevInfo(void);
    void setExtCrystalUse(bool usextal);
    //    void getSystemStatus(uint8_t *system_status,
    //            uint8_t *self_test_result,
    //            uint8_t *system_error);
    //    void displaySystemStatus(void);
    void getCalibration(uint8_t* system, uint8_t* gyro, uint8_t* accel, uint8_t* mag);
    
    imu::Vector<3> getVector(adafruit_vector_type_t vector_type);
    //    imu::Quaternion getQuat(void);
    //    int8_t getTemp(void);

    bool getEvent(sensors_event_t *event);


private:

    uint8_t read8(adafruit_bno055_reg_t);
    bool write8(adafruit_bno055_reg_t, uint8_t value);
    bool readLen(adafruit_bno055_reg_t, uint8_t* buffer, uint8_t len);

    uint8_t I2CreadByte_(uint8_t address, uint8_t subAddress);
    uint8_t I2CreadBytes_(uint8_t address, uint8_t subAddress, uint8_t * dest, uint8_t count);

    bool I2CwriteByte_(uint8_t address, uint8_t subAddress, uint8_t data);


    int fd_;
    adafruit_bno055_opmode_t mode_;

};

#endif /* BNO055_H */

