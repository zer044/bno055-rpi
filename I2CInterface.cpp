/////////////////////////////////////////////////////////////////////
// I2CInterface class
//  provides interface to gumstix I2C interface
//
// Last modified by:
// $Author: simon.willcox $
// $Date: 2017-03-29 16:56:48 +0100 (Wed, 29 Mar 2017) $
// $Revision: 39301 $
//
// Author: Simon
// 
// Blue Bear Systems Research Ltd
//
/////////////////////////////////////////////////////////////////////

#include <iostream>
#include <sstream>

#include <fcntl.h>
#include <unistd.h>
#include <errno.h>
#include <iostream> 
#include <sys/ioctl.h>
#include <linux/i2c.h>
#include <linux/i2c-dev.h>

#include "I2CInterface.h"
//#include "Logger.h"


I2CInterface::I2CInterface()
{
    // open the i2c interface to snap
    i2cFile_ = open("/dev/i2c-1", O_RDWR);
    if (i2cFile_ < 0)
    {
        std::cerr << "error: Couldn't open the i2c interface" << std::endl;
        exit(EXIT_FAILURE);    // @TODO is this the best thing to do here?
    }
}


I2CInterface::~I2CInterface()
{}


// the one and only
I2CInterface& I2CInterface::theI2CInterface()
{
    static I2CInterface i2c;
    return i2c;
}


// send data on our interface to the given slave
// these can fail if the underlying hardware has failed or if there is no hardware
// so don't give up, just issue a warning
bool I2CInterface::send(unsigned char slaveAddress, const unsigned char* buf, int length)
{
    // form the data message
    struct i2c_msg messages[1];
    messages[0].addr  = slaveAddress >> 1;
    messages[0].flags = 0;
    messages[0].len = length;
    messages[0].buf = const_cast<unsigned char*>(buf);

    // build up the packet structure and send it
    struct i2c_rdwr_ioctl_data packets;
    packets.msgs = messages;
    packets.nmsgs = 1;
    
    int ret;
    {
        std::lock_guard<std::mutex> lk(mutex_);
        ret = ioctl(i2cFile_, I2C_RDWR, &packets);
    }
    if (ret < 0)
    {
        std::ostringstream os;
        os << "Warning: sending to i2c, slave = " << static_cast<int>(slaveAddress) << ", ret = " << ret << ", buflen = " << length << ", err = " << errno;
        //Logger::log(os.str());
        return false;
    }
    return true;
}


// receive data on our interface from the given slave
bool I2CInterface::receive(unsigned char slaveAddress, unsigned char bufStart, unsigned char* buf, int length)
{
    // send the start register address we want to read from
    unsigned char outbuf = bufStart;
    struct i2c_msg messages[2];
    messages[0].addr = slaveAddress >> 1;
    messages[0].flags = 0;
    messages[0].len = sizeof (outbuf);
    messages[0].buf = &outbuf;

    // read the registers
    messages[1].addr = slaveAddress >> 1;
    messages[1].flags = I2C_M_RD; //| I2C_M_NOSTART;
    messages[1].len = length;
    messages[1].buf = buf;

    // build the packets and send it
    struct i2c_rdwr_ioctl_data packets;
    packets.msgs = messages;
    packets.nmsgs = 2;

    int ret;
    {
        std::lock_guard<std::mutex> lk(mutex_);
        ret = ioctl(i2cFile_, I2C_RDWR, &packets);
    }
    if (ret < 0)
    {
        std::ostringstream os;
        os << "warning: getting i2c data, slave = " << static_cast<int>(slaveAddress) << ", ret = " << ret << ", buflen = " << length << ", err = " << errno;
       //Logger::log(os.str());
        return false;
    }
    return true;
}
