/////////////////////////////////////////////////////////////////////
// I2CInterface class
//  provides interface to gumstix I2C interface
//
// Last modified by:
// $Author: simon.willcox $
// $Date: 2017-03-21 16:03:48 +0000 (Tue, 21 Mar 2017) $
// $Revision: 39220 $
//
// Author: Simon
// 
// Blue Bear Systems Research Ltd
//
/////////////////////////////////////////////////////////////////////

#ifndef I2CINTERFACE_H_
#define I2CINTERFACE_H_

#include <string>
#include <mutex>


class I2CInterface
{
public:
    // only one of these ever
    static I2CInterface& theI2CInterface();
    
    virtual ~I2CInterface();

    // send and receive
    bool send(unsigned char slaveAddress, const unsigned char* buf, int length);
    bool receive(unsigned char slaveAddress, unsigned char bufStart, unsigned char* buf, int length);
    
private:
    // can't just make these willy-nilly
    I2CInterface();
    I2CInterface(const I2CInterface&);
            
    // handle to I2C file interface, only one of these
    int i2cFile_;
        
    // make us thread safe
    std::mutex mutex_;
    
};

#endif /*I2CINTERFACE_H_*/
