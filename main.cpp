/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

/* 
 * File:   main.cpp
 * Author: narshil.vaghjiani
 *
 * Created on 02 May 2018, 09:39
 */

#include <cstdlib>
#include <iostream>
#include <stdint.h>
#include <wiringPi.h>
#include <fstream>
#include "BNO055.h"


using namespace std;
using namespace bno;

/*
 * 
 */
int main(int argc, char** argv)
{
    cout << "\r Start! " << endl;
  //  ofstream myfile;
  //  myfile.open("bnodata.txt",fstream::out);

    BNO055 imu1;
    sensors_event_t event;
    if (imu1.begin(OPERATION_MODE_IMUPLUS))
    {
        cout << "IMU initialised \r" << endl;
    }
    else
    {
        cout << "\r Failed to initialise IMU" << endl;
    }

    imu1.setExtCrystalUse(true);

    uint8_t sys = 0;
    uint8_t gyro = 0;
    uint8_t accel = 0;
    uint8_t mag = 0;

    while (true)
    {
        imu1.getCalibration(&sys, &gyro, &accel, &mag);

        cout << "System: " << (unsigned int) sys << " \r" << endl;
        cout << "Gyro: " << (unsigned int) gyro << " \r" << endl;
        cout << "Accel: " << (unsigned int) accel << " \r" << endl;
        cout << "Mag: " << (unsigned int) mag << " \r" << endl;

        if (sys >= 0x02)
        {
            cout << "System calibrated.." << endl;
            break;
        }
    }
    while (true)
    {
        imu1.getEvent(&event);
        cout << "X: " << event.orientation.x << "\r" << endl;
        cout << "Y: " << event.orientation.y << "\r" << endl;
        cout << "Z: " << event.orientation.z << "\r" << endl;

        delay(25);
    }

   // myfile.close();
    return 0;
}

